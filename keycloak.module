<?php

/**
 * @file
 * Hook implementations of the Keycloak module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\keycloak\Service\KeycloakServiceInterface;
use Drupal\openid_connect\OpenIDConnectClientEntityInterface;
use Drupal\user\UserInterface;

/**
 * Implements hook_modules_installed().
 */
function keycloak_modules_installed($modules) {
  // Whether the keycloak module was installed.
  if (in_array('keycloak', $modules)) {
    // Show configuration page hint.
    $settings = Url::fromRoute('openid_connect.admin_settings')->toString();
    \Drupal::messenger()->addStatus(t('You can now enable Keycloak OpenID Connect sign in at the <a href=":settings">OpenID Connect settings</a> page.', [
      ':settings' => $settings,
    ]));
  }
}

/**
 * Implements hook_help().
 */
function keycloak_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the keycloak module.
    case 'help.page.keycloak':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Keycloak module allows you to authenticate your users against a Keycloak authentication server. You can enable the Keycloak client within the <a href=":settings">OpenID Connect settings</a> page. For more information, see the <a href=":documentation">online documentation for the Keycloak module</a>.', [
        ':settings' => Url::fromRoute('openid_connect.admin_settings')->toString(),
        ':documentation' => 'https://www.drupal.org/docs/8/modules/keycloak-openid-connect',
      ]) . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Login to Drupal using Keycloak OpenID Connect') . '</dt>';
      $output .= '<dd>' . t('Your Drupal users can use an external Keycloak authentication server to register and login to your Drupal site.') . '</dd>';
      $output .= '<dt>' . t('Use Keycloak as Drupal authentication back-end') . '</dt>';
      $output .= '<dd>' . t("Optionally replace Drupal's authentication back-end entirely with Keycloak.") . '</dd>';
      $output .= '<dt>' . t('Synchronize user fields with OpenID claims') . '</dt>';
      $output .= '<dd>' . t("During login and user registration you can synchronize user attributes with OpenID claims using the OpenID Connect module's claim mapping.") . '</dd>';
      $output .= '<dt>' . t('Synchronize email address changes from within Keycloak') . '</dt>';
      $output .= '<dd>' . t("If the user's email address changed in Keycloak, you can synchronize this change with the Drupal user's email address.") . '</dd>';
      $output .= '<dt>' . t('Single sign-out') . '</dt>';
      $output .= '<dd>' . t("Optionally end a user's Keycloak session, when logging out from Drupal, or automatically log out from Drupal when the Keycloak session ends.") . '</dd>';
      $output .= '<dt>' . t('Multi-language support') . '</dt>';
      $output .= '<dd>' . t("When using a multi-language Drupal site and a multi-language Keycloak authentication server, you can forward language parameters to the login and register pages and map Keycloak locales to the Drupal user's language settings.") . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Implements hook_openid_connect_userinfo_save().
 */
function keycloak_openid_connect_userinfo_save(UserInterface $account, array $context) {
  $openid_connect_client = \Drupal::entityTypeManager()->getStorage('openid_connect_client')->load($context['plugin_id']);
  if (!$openid_connect_client instanceof OpenIDConnectClientEntityInterface) {
    return;
  }

  $openid_connect_client->getPlugin()->applyRoleRules($account, $context['userinfo']);
}

/**
 * Implements hook_openid_connect_post_authorize().
 *
 * Stores the Keycloak session_state parameter to the logged in user's
 * session.
 */
function keycloak_openid_connect_post_authorize(UserInterface $account, array $context) {
  $tokens = $context['tokens'] ?? [];
  $plugin_id = $context['plugin_id'] ?? [];
  $user_data = $context['user_data'] ?? [];

  // Whether the client used for authentication was not keycloak.
  if (empty($plugin_id) || $plugin_id !== 'keycloak') {
    // Nothing to do. Bail out.
    return;
  }

  // Whether a session_state was provided by the IdP.
  if (!isset($user_data['session_state'])) {
    return;
  }

  $session_info = [
    KeycloakServiceInterface::KEYCLOAK_SESSION_ACCESS_TOKEN => $tokens['access_token'],
    KeycloakServiceInterface::KEYCLOAK_SESSION_REFRESH_TOKEN => $tokens['refresh_token'],
    KeycloakServiceInterface::KEYCLOAK_SESSION_ID_TOKEN => $tokens['id_token'],
    // Storage the session ID (OpenID Connect 'session_state').
    KeycloakServiceInterface::KEYCLOAK_SESSION_CLIENT_ID => $user_data['aud'],
    // Storage the client ID (OpenID Connect audience = 'aud').
    KeycloakServiceInterface::KEYCLOAK_SESSION_SESSION_ID => $user_data['session_state'],
  ];

  /** @var \Drupal\keycloak\Service\KeycloakServiceInterface $keycloak */
  Drupal::service('keycloak.keycloak')->setSessionInfo($session_info);
}

/**
 * Implements hook_page_attachments_alter().
 *
 * Add our Keycloak session check to the page, if the user was logged
 * in with Keycloak and SSO is enabled.
 */
function keycloak_page_attachments_alter(array &$build) {
  /* @var $keycloak \Drupal\keycloak\Service\KeycloakServiceInterface */
  $keycloak = \Drupal::service('keycloak.keycloak');

  // Whether the current user is not a Keycloak user or check session
  // is disabled.
  if (!$keycloak->isKeycloakUser() || !$keycloak->isCheckSessionEnabled()) {
    return;
  }

  $session_info = $keycloak->getSessionInfo([
    KeycloakServiceInterface::KEYCLOAK_SESSION_CLIENT_ID,
    KeycloakServiceInterface::KEYCLOAK_SESSION_SESSION_ID,
  ]);
  // Whether no session parameters were found.
  if (
    empty($session_info[KeycloakServiceInterface::KEYCLOAK_SESSION_CLIENT_ID]) ||
    empty($session_info[KeycloakServiceInterface::KEYCLOAK_SESSION_SESSION_ID])
  ) {
    return;
  }

  // Attach session check JS and session information to the page.
  $build['#attached']['library'][] = 'keycloak/keycloak-session';
  $build['#attached']['drupalSettings']['keycloak'] = [
    'enableSessionCheck' => TRUE,
    'sessionCheckInterval' => $keycloak->getCheckSessionInterval(),
    'sessionCheckIframeUrl' => $keycloak->getCheckSessionIframeUrl(),
    'logoutUrl' => Url::fromRoute('user.logout', [], [
      'query' => [
        'op_initiated' => 1,
      ],
      'absolute' => TRUE,
    ])->toString(),
    'logout' => FALSE,
    'clientId' => $session_info[KeycloakServiceInterface::KEYCLOAK_SESSION_CLIENT_ID],
    'sessionId' => $session_info[KeycloakServiceInterface::KEYCLOAK_SESSION_SESSION_ID],
  ];
}
